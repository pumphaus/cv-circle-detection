#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Everything here is tuned for images of size 326 x 245

@author: pumphaus
"""

import cv2
import numpy as np
import scipy.stats as stats

def preprocess_image(im, gamma=1, alpha=1, beta=0):
    size = np.array(im.shape[:2])
    factor = np.mean(np.array([326, 245]) * 1.7 / size)
    newsize = tuple(np.round(size * factor)[::-1].astype(int))
    im = cv2.resize(im, newsize, interpolation=cv2.INTER_AREA)

#    im = np.clip(np.power(im / 255, gamma) * 255, 0, 255).astype('uint8')
#    im = np.clip(alpha * im.astype(float) + beta, 0, 255).astype('uint8')

    return im

def hls_threshold(im, hls_min, hls_max):
    im_hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HLS)
    mask = cv2.inRange(im_hsv, hls_min, hls_max)
    thresholded = cv2.bitwise_and(im, im, mask=mask)
    return thresholded

def detect_small_circles(im, minArea=120, maxArea=5000):
    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()
    params.blobColor = 255

    # Change thresholds
    params.minThreshold = 10
    params.thresholdStep = 1
    params.maxThreshold = 255

    # Filter by Area.
    params.filterByArea = True
    params.minArea = minArea
    params.maxArea = maxArea

    # Filter by Circularity
    params.filterByCircularity = False
    params.minCircularity = 0.1

    # Filter by Convexity
    params.filterByConvexity = False
    params.minConvexity = 0.8

    # Filter by Inertia
    params.filterByInertia = False
    params.minInertiaRatio = 0.01

    # Set up the detector with default parameters.
    detector = cv2.SimpleBlobDetector_create(params)

    # Detect blobs.
    keypoints = detector.detect(im)
    return keypoints

def detect_large_circle(im, minArea=10000, maxArea=np.inf):
    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()
    params.blobColor = 255
    # Change thresholds
    params.minThreshold = 10
    params.thresholdStep = 1
    params.maxThreshold = 250
    # Filter by Area.
    params.filterByArea = True
    params.minArea = minArea
    params.maxArea = maxArea
    # Filter by Circularity
    params.filterByCircularity = True
    params.minCircularity = 0.3
    # Filter by Convexity
    params.filterByConvexity = False
    params.minConvexity = 0.8
    # Filter by Inertia
    params.filterByInertia = False
    params.minInertiaRatio = 0.01
    # Set up the detector with default parameters.
    detector = cv2.SimpleBlobDetector_create(params)
    # Detect blobs.
    keypoints = detector.detect(im)
    return keypoints[0]

def is_inside(point, circle):
    return np.linalg.norm(np.array(point.pt) - np.array(circle.pt)) <= circle.size / 2

def closest_to_center(keypoints, circle):
    if len(keypoints) == 0:
        return -1

    dists = []
    for pt in keypoints:
        dists.append(np.linalg.norm(np.array(pt.pt) - np.array(circle.pt)))
    return np.argmin(dists)

def get_angle(z):
    a = np.degrees(np.angle(z))
    if a < 0:
        a += 360
    return a

def angle_between(z1, z2, wrap=True):
    a = abs(get_angle(z1) - get_angle(z2))
    if not wrap:
        return a

    if a > 180:
        a = 360 - a
    return a

def get_normalized_data(keypoints, boundary):
    r_bound = boundary.size / 2
    center = complex(*keypoints.pop(0).pt)

    data = []
    for pt in keypoints:
        z = complex(*pt.pt) - center
        z = z.conjugate()
        z /= r_bound
        data.append(z)

    delta_angle = angle_between(data[0], data[1], wrap=False)

    if delta_angle > 180:
        data.sort(key=lambda x: get_angle(x))
    else:
        data.sort(key=lambda x: -get_angle(x))

    return data
