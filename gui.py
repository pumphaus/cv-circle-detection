#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 23:03:14 2019

@author: pumphaus
"""

import cv2
import numpy as np
import sys

im = cv2.imread(sys.argv[1])
size = np.array(im.shape[:2])
factor = np.mean(np.array([326, 245]) * 1.1 / size)
newsize = tuple(np.round(size * factor)[::-1].astype(int))
im = cv2.resize(im, newsize)

hls_min = np.array([8, 80, 79], dtype=int)
hls_max = np.array([29, 130, 190], dtype=int)

im_hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HLS)

def cb_hueMin(v):
    hls_min[0] = v
    update_image()

def cb_hueMax(v):
    hls_max[0] = v
    update_image()

def cb_lightnessMin(v):
    hls_min[1] = v
    update_image()

def cb_lightnessMax(v):
    hls_max[1] = v
    update_image()

def cb_saturationMin(v):
    hls_min[2] = v
    update_image()

def cb_saturationMax(v):
    hls_max[2] = v
    update_image()

def update_image():
    mask = cv2.inRange(im_hsv, hls_min, hls_max)
    res = cv2.bitwise_and(im, im, mask=mask)
    cv2.imshow("HSL", res)

cv2.namedWindow("HSL")
cv2.createTrackbar("hue_min", "HSL", hls_min[0], 359, cb_hueMin)
cv2.createTrackbar("hue_max", "HSL", hls_max[0], 359, cb_hueMax)
cv2.createTrackbar("lightness_min", "HSL", hls_min[1], 255, cb_lightnessMin)
cv2.createTrackbar("lightness_max", "HSL", hls_max[1], 255, cb_lightnessMax)
cv2.createTrackbar("saturation_min", "HSL", hls_min[2], 255, cb_saturationMin)
cv2.createTrackbar("saturation_max", "HSL", hls_max[2], 255, cb_saturationMax)

update_image()

while True:
    cv2.waitKey(0)
