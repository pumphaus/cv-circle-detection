#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 11:56:22 2019

@author: pumphaus
"""

from PyQt5.QtCore import Qt, QPointF, QPoint
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QGridLayout, QLineEdit, QPushButton, QFileDialog, QMessageBox
from PyQt5.QtGui import QImage, QPixmap, QPainter, QPen
import sys
from glob import glob
import util
import cv2
import enum
import os
import numpy as np

class Tags(enum.Enum):
    Center = 0
    F = 1
    FF = 2
    W = 3
    WW = 4

class CircleSelector(QLabel):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ptradius = 30

    def load_image(self, file):
        print("Load", file)
        im = cv2.imread(file)
        im = util.preprocess_image(im)

        height, width, channel = im.shape
        bytesPerLine = 3 * width
        self.fname = file
        self.qImg = QImage(im.data, width, height, bytesPerLine, QImage.Format_RGB888)
        self.boundary = util.detect_large_circle(im, minArea=70000)
        self.setPixmap(QPixmap.fromImage(self.qImg.rgbSwapped()))
        self.curPoints = {}
        self.update()

    def mouseReleaseEvent(self, event):
        evPt = complex(event.x(), event.y())

        existingPt = None

        for pt in self.curPoints.keys():
            if abs(pt - evPt) <= self.ptradius / 2:
                existingPt = pt
                break

        if event.button() == Qt.RightButton:
            if existingPt is not None:
                del self.curPoints[existingPt]
                self.update()
            return

        if existingPt is None:
            existingPt = evPt
            self.curPoints[existingPt] = 0
        else:
            self.curPoints[existingPt] += 1

        try:
            Tags(self.curPoints[existingPt])
        except ValueError:
            del self.curPoints[existingPt]

        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)

        painter.drawPixmap(0, 0, self.pixmap())

        bpen = QPen()
        bpen.setColor(Qt.green)
        bpen.setWidth(2.5)
        painter.setPen(bpen)
        x, y = self.boundary.pt
        painter.drawEllipse(QPointF(x, y), self.boundary.size / 2,
                            self.boundary.size / 2)

        for pt, tag in self.curPoints.items():
            pen = QPen()
            pen.setWidth(2.5)

            tag = Tags(tag)
            if tag == Tags.Center:
                pen.setColor(Qt.blue)
            else:
                pen.setColor(Qt.red)

            painter.setPen(pen)

            painter.drawEllipse(pt.real - self.ptradius / 2, pt.imag - self.ptradius / 2,
                                self.ptradius, self.ptradius)

            painter.drawText(pt.real + self.ptradius / 2, pt.imag + self.ptradius / 2, tag.name)


app = QApplication(sys.argv)

directory = QFileDialog.getExistingDirectory(None, "Ordner mit Bildern auswählen")
if not directory:
    exit(1)

paths = sorted(glob(directory + '/*.JPG'))

it = iter(paths)

datalines = []

w = QWidget()
sel = CircleSelector()
sel.load_image(next(it))
info = QLineEdit()
nextbtn = QPushButton("&Next")
quitbtn = QPushButton ("&Quit")

class MyKeyPoint:
    def __init__(self, cpt, tag):
        self.pt = [cpt.real, cpt.imag]
        self.tag = tag

def get_normalized_data(keypoints, boundary):
    r_bound = boundary.size / 2
    center = complex(*keypoints.pop(0).pt)

    data = []
    tags = []
    for pt in keypoints:
        z = complex(*pt.pt) - center
        z = z.conjugate()
        z /= r_bound
        data.append(z)
        tags.append(pt.tag)

    return data, tags

def save_and_quit():
    fname = QFileDialog.getSaveFileName(None, "Zieldatei auswählen", "", "Textdateien (*.txt)")[0]

    if not fname:
        return

    print("Save to", fname)

    with open(fname, 'w') as file:
        print("# dateiname,typ1,abstand1,winkel1,typ2,abstand2,winkel2,winkel_dazwischen,optionale_infos", file=file)
        for line in datalines:
            print(line, file=file)

    app.quit()

def load_next():
    if len(sel.curPoints) != 3:
        QMessageBox.warning(w, "Falsche Anzahl an Punkten", "Falsche Anzahl an Punkten")
        return

    centerpts = [ pt for pt, tag in sel.curPoints.items() if tag == Tags.Center.value ]

    if len(centerpts) != 1:
        QMessageBox.warning(w, "Falsche Anzahl an Mittelpunkten", "Falsche Anzahl an Mittelpunkten")
        return

    center = centerpts[0]

    keypoints = [ MyKeyPoint(pt, Tags(tag)) for pt, tag in sel.curPoints.items() if tag != Tags.Center.value ]
    keypoints.insert(0, MyKeyPoint(center, Tags.Center))

    data, tags = get_normalized_data(keypoints, sel.boundary)

    # Format: dateiname,typ1,abstand1,winkel1,typ2,abstand2,winkel2,winkel_dazwischen,optionale_infos
    dataline = "{},{},{},{},{},{},{},{},{}".format(
          os.path.splitext(os.path.basename(sel.fname))[0],
          tags[0].name, np.abs(data[0]), util.get_angle(data[0]),
          tags[1].name, np.abs(data[1]), util.get_angle(data[1]),
          util.angle_between(data[0],data[1]),
          info.text()
    )

    datalines.append(dataline)

    try:
        info.setText("")
        sel.load_image(next(it))
    except StopIteration:
        nextbtn.setEnabled(False)
        save_and_quit()

quitbtn.clicked.connect(save_and_quit)
nextbtn.clicked.connect(load_next)

layout = QGridLayout(w)
layout.addWidget(sel, 0, 0, 2, 1)
layout.addWidget(info, 0, 1, 1, 2)
layout.addWidget(quitbtn, 1, 1)
layout.addWidget(nextbtn, 1, 2)

w.resize(800, 600)
w.show()

exit(app.exec_())
