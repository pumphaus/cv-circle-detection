#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 12:01:09 2019

@author: pumphaus
"""

from detect_blobs import detect_blobs
from util import get_normalized_data, get_angle, angle_between
import numpy as np
import sys
import os

print("# index,dist1,dist2,angle1,angle2,delta_angle")

for i, file in enumerate(sys.argv[1:]):
    print("{}/{}, {}".format(i + 1, len(sys.argv) - 1, file), file=sys.stderr)

    keypoints, boundary = detect_blobs(file)
    data = get_normalized_data(keypoints, boundary)
    print("{},{},{},{},{},{}".format(
          os.path.splitext(os.path.basename(file))[0],
          np.abs(data[0]), np.abs(data[1]),
          get_angle(data[0]), get_angle(data[1]),
          angle_between(data[0],data[1])
    ))
