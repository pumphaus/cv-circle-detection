#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 19:19:14 2018

@author: pumphaus
"""

#%%
import cv2
import numpy as np
import matplotlib.pyplot as plt
import util

#%%

def interactively_select_points(filename, im, keypoints, boundary):
    winname = "Emojis auswählen für {}".format(filename)
    cv2.namedWindow(winname)

    avg_radius = 9 if len(keypoints) == 0 \
                   else np.mean([ x.size / 2 for x in keypoints])

    def update():
        copy_im = np.copy(im)

        cv2.circle(copy_im, tuple(np.array(boundary.pt).astype(int)),
                   round(boundary.size / 2), (255, 0, 0), 1)

        if len(keypoints) == 0:
            cv2.imshow(winname, copy_im)
            return

        center_idx = util.closest_to_center(keypoints, boundary)

        for idx, x in enumerate(keypoints):
            i, j = x.pt
            i, j = round(i), round(j)

            color = (0, 255, 0)
            if idx == center_idx:
                color = (0, 0, 255)

            cv2.circle(copy_im, (i, j), round(x.size / 2), color, 1)
        cv2.imshow(winname, copy_im)

    update()

    def on_mouse(event, x, y, flags, param):
        if (event != cv2.EVENT_LBUTTONUP):
            return

        clickpt = cv2.KeyPoint(x=x - 2, y=y - 3, _size=avg_radius * 2)
        container = next((x for x in keypoints if util.is_inside(clickpt, x)), None)
        try:
            keypoints.remove(container)
        except:
            keypoints.append(clickpt)

        update()

    cv2.setMouseCallback(winname, on_mouse)

    while (cv2.waitKey(0) != 13): pass
    cv2.destroyWindow(winname)

    return keypoints

def detect_blobs(file):
    # Read image
    im = cv2.imread(file)

    im = util.preprocess_image(im)

    # originally: np.array([8, 80, 79], dtype=int)
    hls_min = np.array([8, 0, 79], dtype=int)
    hls_max = np.array([29, 130, 190], dtype=int)
    thresholded = util.hls_threshold(im, hls_min, hls_max)


    #%%

    # Detect blobs.
    keypoints = util.detect_small_circles(thresholded)
    boundary = util.detect_large_circle(im)
    keypoints = [ p for p in keypoints if util.is_inside(p, boundary)]

    keypoints = interactively_select_points(file, im, keypoints, boundary)
    while len(keypoints) != 3:
        keypoints = interactively_select_points(file, im, keypoints, boundary)

    center_idx = util.closest_to_center(keypoints, boundary)
    keypoints.insert(0, keypoints.pop(center_idx))

    return keypoints, boundary
